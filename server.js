const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./src/routes/index')
const app = express();
const port = 4000;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
routes.setRoute(app);
app.get('/', (req, res) => {
    res.send('Testing API')
});

app.listen(port, () => {
    console.log(`App started on port ${port}!`)
});
