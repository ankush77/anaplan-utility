const ReadRepository = require('./read-data-repository');

async function getAll() {
    return await ReadRepository.getAll().then(data=>{
        let csv = data.map(row => Object.values(row));
            csv.unshift(Object.keys(data[0]));
            return csv.join('\n');
    });
}

async function getList() {
    return await ReadRepository.getList();
}


module.exports = {
    getAll,
    getList
}