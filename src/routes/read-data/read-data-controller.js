const anaplanRequest = require('../../Anaplan/anaplan-request');
const anaplanAuthentication = require('../../Anaplan/anaplan-auth');
const anaplanConfigs = require('../../configs/anaplan')
const router = require('express').Router();
const ReadService = require('./read-data-service')

router.get("/", getAll);
router.get("/list", getList);
router.get("/fetch", getData);


//fetch data from MSSQL server and push into Anaplan
async function getAll(req, res, next) {
    await ReadService.getAll().then( async result => {
        let token='';
        anaplanAuthentication.authentication(anaplanConfigs.ANAPLAN_CONFIG_USERNAME,anaplanConfigs.ANAPLAN_CONFIG_PASSWORD)
        .then(anaplanToken=>{
            token=anaplanToken;
            return anaplanRequest.uploadFile(token,result)})
        .then(uploadStatus=>anaplanRequest.completeUploadFile(token))
        .then(completeFileStatus=>anaplanRequest.triggerImportID(token))
        .then(triggerStatus=>res.send(triggerStatus))
        .catch(error=>res.status(400).send(error));

    })
        .catch(error => res.status(404).send(error));
}

//get data from Anaplan
async function getData(req, res, next) {
        let token='';
        anaplanAuthentication.authentication(anaplanConfigs.ANAPLAN_CONFIG_USERNAME,anaplanConfigs.ANAPLAN_CONFIG_PASSWORD)
        .then(anaplanToken=>{
            token=anaplanToken;
            return anaplanRequest.triggerExportAction(token)})
        .then(exportAction=>anaplanRequest.getExportActionStatus(token,exportAction))
        .then(exportStatus=>anaplanRequest.getFileData(token))
        .then(fileData=>res.send(fileData))
        .catch(error=>res.status(400).send(error));
}

//get list of tables in MSSQL
async function getList(req, res, next) {
    await ReadService.getList().then(result => res.json(result))
        .catch(error => res.status(404).send(error));
}

module.exports = router;