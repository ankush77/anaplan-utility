const poolPromise = require('../../database/index');
const sql = require('mssql')

async function getAll() {
    var query = "SELECT * FROM DRFTable";
    try {
        const pool = await poolPromise
        const result = await pool.request()
            .query(query);
        return result.recordset;
    } catch (err) {
        return Promise.reject(err.message);
    }
}

async function getList() {
    var query = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE'";
    try {
        const pool = await poolPromise
        const result = await pool.request()
            .query(query);
        return result.recordset;
    } catch (err) {
        return Promise.reject(err.message);
    }
}

module.exports = {
    getAll,
    getList
}