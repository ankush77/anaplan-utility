function setRoute(app) {
    app.use("/sql", require("./read-data/read-data-controller"));
};

module.exports = {
    setRoute
};