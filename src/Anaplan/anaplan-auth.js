var request = require('request');
const anaplanConfigs = require('../configs/anaplan')


anaplanAuthentication = {
    
    authentication: (username,password) => {
        return new Promise(function(resolve, reject) {

            var options = {
                'method': 'POST',
                'url': anaplanConfigs.ANAPLAN_CONFIG_AUTH_URL+'/token/authenticate',
                'headers': {
                  'Content-Type': 'application/json',
                  'Authorization': "Basic " + Buffer.from(username + ":" + password).toString("base64")
                }
              };
              request(options, function (error, response) {
                if (error) reject(error);
                console.log("Ticket:",response.body);
                resolve(response.body);
              });
        });
    },
};

module.exports = anaplanAuthentication;