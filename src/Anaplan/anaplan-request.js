var https = require('https');
var request = require('request');
const anaplanConfigs = require('../configs/anaplan')


anaplanRequest = {
    
    uploadFile: (token,data) => {
        return new Promise(function(resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let  options = {
                hostname: anaplanConfigs.ANAPLAN_CONFIG_BASE_URL,
                port: 443,
                path: '/2/0/workspaces/'+anaplanConfigs.ANAPLAN_CONFIG_WORKSPACE_ID+'/models/'+anaplanConfigs.ANAPLAN_CONFIG_MODEL_ID+'/files/'+anaplanConfigs.ANAPLAN_CONFIG_FILE_ID+'/chunks/0',
                method: "PUT",
                headers: {
                    Authorization: 'AnaplanAuthToken '+anaplanToken.tokenInfo.tokenValue,
                    "Content-Type": "application/octet-stream",
                },

            }
            let request = https.request(options, function(response) {
                console.log(`Upload File StatusCode: ${response.statusCode}`);
                resolve(response);
                response.on('data', d => {
                    console.log("Data:",d);
                    process.stdout.write(d);
                })
            });
             request.write(data.toString());   // write data in anaplan 
             request.on('error', (e) => {
              console.log("Error in data:",e);
             });
            request.end();
        });
    },
    completeUploadFile: (token) => {

        return new Promise(function(resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
            'method': 'POST',
            'url': 'https://'+anaplanConfigs.ANAPLAN_CONFIG_BASE_URL+'/2/0/workspaces/'+anaplanConfigs.ANAPLAN_CONFIG_WORKSPACE_ID+'/models/'+anaplanConfigs.ANAPLAN_CONFIG_MODEL_ID+'/files/'+anaplanConfigs.ANAPLAN_CONFIG_FILE_ID+'/complete',
            'headers': {
              'Authorization': 'AnaplanAuthToken '+anaplanToken.tokenInfo.tokenValue,
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "chunkCount": 1
            })
          
          };
          request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log(response.body);
            resolve(response);
          });
          
        });
    },
    triggerImportID: (token) => {
     
        return new Promise(function(resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
            'method': 'POST',
            'url': 'https://'+anaplanConfigs.ANAPLAN_CONFIG_BASE_URL+'/2/0/workspaces/'+anaplanConfigs.ANAPLAN_CONFIG_WORKSPACE_ID+'/models/'+anaplanConfigs.ANAPLAN_CONFIG_MODEL_ID+'/imports/'+anaplanConfigs.ANAPLAN_CONFIG_IMPORT_ID+'/tasks',
            'headers': {
              'Authorization': 'AnaplanAuthToken '+anaplanToken.tokenInfo.tokenValue,
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "localeName": "en_US"
            })
          
          };
          request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log(response.body);
            resolve(response);
          });

        });
    },
    triggerExportAction: (token) => {
        return new Promise(function(resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
            'method': 'POST',
            'url': 'https://'+anaplanConfigs.ANAPLAN_CONFIG_BASE_URL+'/2/0/workspaces/'+anaplanConfigs.ANAPLAN_CONFIG_WORKSPACE_ID+'/models/'+anaplanConfigs.ANAPLAN_CONFIG_MODEL_ID+'/exports/'+anaplanConfigs.ANAPLAN_CONFIG_EXPORT_ID+'/tasks',
            'headers': {
              'Authorization': 'AnaplanAuthToken '+anaplanToken.tokenInfo.tokenValue,
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "localeName": "en_US"
            })
          
          };
          request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log(response.body);
            resolve(response.body);
          });

        });
    },

    getExportActionStatus: (token,task) => {
        return new Promise(function(resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let exportTask = JSON.parse(task);
            var options = {
              'method': 'GET',
              'url': 'https://'+anaplanConfigs.ANAPLAN_CONFIG_BASE_URL+'/2/0/workspaces/'+anaplanConfigs.ANAPLAN_CONFIG_WORKSPACE_ID+'/models/'+anaplanConfigs.ANAPLAN_CONFIG_MODEL_ID+'/exports/'+anaplanConfigs.ANAPLAN_CONFIG_EXPORT_ID+'/tasks/'+exportTask.task.taskId,
              'headers': {
                'Authorization': 'AnaplanAuthToken '+anaplanToken.tokenInfo.tokenValue,
                'Content-Type': 'application/json'
              }
            };
            request(options, function (error, response) {
              if (error) reject(error);
               let status = JSON.parse(response.body);
              if(status.task.currentStep.toLowerCase().includes("complete"))
              {
               console.log("Export Action Completed");
               resolve(response.body);
              }
              else {
                console.log("Waiting for export action to be completed..");
                anaplanRequest.getExportActionStatus(token,task);}
            });
            
        });
    },
    getFileData: (token) => {
        return new Promise(function(resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
              'method': 'GET',
              'url': 'https://'+anaplanConfigs.ANAPLAN_CONFIG_BASE_URL+'/2/0/workspaces/'+anaplanConfigs.ANAPLAN_CONFIG_WORKSPACE_ID+'/models/'+anaplanConfigs.ANAPLAN_CONFIG_MODEL_ID+'/files/'+anaplanConfigs.ANAPLAN_CONFIG_FILE_ID,
              'headers': {
                'Authorization': 'AnaplanAuthToken ' +anaplanToken.tokenInfo.tokenValue,
                'Content-Type': 'application/json'
              }
            };
            request(options, function (error, response) {
              if (error) throw new Error(error);
              resolve(response.body);
            });
        });
    },
};

module.exports = anaplanRequest;