const HOST = "psslazuresql.database.windows.net";
const USERNAME = "psslazuresa";
const PASSWORD = "TXzW62dswW3Kq9r4";
const DATABASE_NAME = "PSSLAzureSQL";
const INSTANCE_NAME = "MSSQLSERVER01";
const PORT = 1433;

const dbConfig = {
    user: USERNAME,
    password: PASSWORD,
    server: HOST,
    database: DATABASE_NAME,
    options: {
        trustedconnection: true,
        enableArithAbort: true,
        instancename: INSTANCE_NAME
    },
    port: PORT
}

module.exports = dbConfig;